from embedding_as_service.text.encode import Encoder
import pandas as pd


def create_embeddings(df, EMBEDDING, MODEL, MAX_SEQ_LENGTH):
    '''
    Create embedding vectors from abstracts.

    :param df: pandas dataframe containging abstracts
    :param EMBEDDING: string, type of pretrained model to use
    :param MODEL: string, subtype of pretrained model
    :param MAX_SEQ_LENGTH: int, max number of input sequences to use from abstract during embedding
    :return: pandas dataframe with embedding values
    '''
    print('Initializing encoder...')
    en = Encoder(embedding=EMBEDDING, model=MODEL, max_seq_length=MAX_SEQ_LENGTH)

    print('Creating embeddings...')
    embeddings = en.encode(texts=df['abstract'].tolist(), pooling='reduce_mean')

    print('Creating embeddings dataframe...')
    embeddings = pd.DataFrame(embeddings)

    print('Exporting embeddings...')
    embeddings.to_csv('../data/embeddings.csv', index=False)

    print('Finished creating embeddings!')

    return embeddings



