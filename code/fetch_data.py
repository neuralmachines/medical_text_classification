import re
import requests
import pandas as pd
import config


def perform_esearch(term, retmax):
    '''
    Perform eserach.

    :param term: string
    :param retmax: string
    :return: requests response
    '''
    print('Performing esearch...')

    print('Building query for ESearch...')
    query = 'esearch.fcgi?' \
            'db=pubmed' \
            '&retmax=' + retmax + \
            '&term=' + term + \
            '&usehistory=y'

    response = requests.get(config.BASE_URL + query)

    return response


def perform_efetch(response, retmax):
    '''
    Perform efetch.

    :param response: requests response
    :param retmax: string
    :return: requests response
    '''
    print('Performing efetch...')

    print('Parsing QueryKey (this is required to build the query)...')
    query_key = re.search('<QueryKey>(.*)</QueryKey>', response.text).group(1)

    print('Parsing WebEnv (this is required to build the query)...')
    web_env = re.search('<WebEnv>(.*)</WebEnv>', response.text).group(1)

    print('Building query for EFetch...')
    query = 'efetch.fcgi?db=pubmed&retmax=' + retmax + '&query_key=' + query_key + '&WebEnv=' + web_env + '&rettype=xml&retmode=text'

    data = requests.get(config.BASE_URL + query)

    return data


def collect_results(data, label):
    '''
    Collect abstracts into dataframe.

    :param data: requests response in XML format
    :param label: string label to use in classification
    :return: pandas dataframe with abstracts and labels
    '''
    print('Collecting all abstracts from the data fetched to a list...')
    abstracts = re.findall(r'<AbstractText>(.*)</AbstractText>', data.text)

    print('Collecting data in a pandas DataFrame...')
    df = pd.DataFrame({'abstract': abstracts, 'label': label})

    return df


def create_dataset(terms, retmax):
    '''
    Create dataset for classification.

    :param terms: dict containing labels and search terms
    :param retmax: string, maximum number of abstratcs to retain
    :return: pandas dataframe with labels and abstracts
    '''
    print('Creating empty dataframe into which results will be collected...')
    df = pd.DataFrame()

    print('Iterating over search terms and collect results into pandas dataframe...')
    for label, term in terms.items():
        temp_df = pd.DataFrame()
        temp_retmax = retmax
        while temp_df.shape[0] < config.MIN_SAMPLES_BY_CATEGORY:
            print('Collecting abstracts for {}...'.format(label))
            response = perform_esearch(term, temp_retmax)
            data = perform_efetch(response, temp_retmax)
            temp_df = collect_results(data, label)
            print('Number of abstracts for {}: {}'.format(label, temp_df.shape[0]))
            temp_retmax = str(int(int(temp_retmax) * 1.5))
            print(temp_retmax)
            if int(temp_retmax) > 25000:
                break
        df = df.append(temp_df.head(n=config.MIN_SAMPLES_BY_CATEGORY))

    print('Resetting index...')
    df = df.reset_index(drop=True)

    print('Exporting data...')
    df.to_csv('../data/data.csv', index=False)

    print('Finished successfully!')

    return df

