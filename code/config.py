# Base URL
BASE_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'

# Set maximum items to retain
RETMAX = '5000'
MIN_SAMPLES_BY_CATEGORY = 2500

# Set dictionary with labels and search terms
TERMS = {'congenital_abnormalities': 'Congenital Abnormalities[mesh]+NOT+Drug-Related Side Effects and Adverse Reactions[mesh]',
         'drug_related_side_effects': 'Drug-Related Side Effects and Adverse Reactions[mesh]+NOT+Congenital Abnormalities[mesh]',
         'drug_usage_congenital_abnormalities': 'Congenital Abnormalities[mesh]+AND+Drug-Related Side Effects and Adverse Reactions[mesh]',
         'psycholinguistics': 'Psycholinguistics[mesh]'}

TERMS = {'others': """
                    "Chemically-Induced Disorders"[Mesh]
                    +NOT+"Drug-Related Side Effects and Adverse Reactions"[Mesh]
                    +NOT+"Congenital Abnormalities"[Mesh]
                    +AND+"english"[Language]""",
         'congenital_abnormalities': 'Congenital Abnormalities[mesh]+NOT+Drug-Related Side Effects and Adverse Reactions[mesh]+AND+"english"[Language]',
         'drug_related_side_effects': 'Drug-Related Side Effects and Adverse Reactions[mesh]+NOT+Congenital Abnormalities[mesh]+AND+"english"[Language]',
         'drug_usage_congenital_abnormalities': 'Congenital Abnormalities[mesh]+AND+Drug-Related Side Effects and Adverse Reactions[mesh]+AND+"english"[Language]',

         }

EMBEDDING = 'albert'
MODEL = 'albert_xxlarge'
MAX_SEQ_LENGTH = 180
PERPLEXITY = [5, 30, 50, 100]

MULTICLASS_FLAG = False
BASELINE_MODEL = True
INPUT_DIM = 4

# Change output dim according to model embedding dimension
OUTPUT_DIM = 4096
EPOCHS = 100
BATCH_SIZE = 5

