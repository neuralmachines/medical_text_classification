from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import config


def perform_pca(embeddings):
    '''
    Perform PCA to reduce embedding vectors dimension to 50.

    :param embeddings: pandas dataframe containing embeddings
    :return: numpy array
    '''
    print('Initialising PCA...')
    pca = PCA(n_components=50)

    print('Fitting embeddings...')
    pca.fit(embeddings)

    print('Transforming embeddings...')
    embeddings_pca_transformed = pca.transform(embeddings)

    return embeddings_pca_transformed


def perform_tsne(embeddings_pca_transformed, perplexity):
    '''
    Perform tSNE on PCA reduced embedding data.

    :param embeddings_pca_transformed: numpy array
    :param perplexity: int
    :return: numpy array
    '''
    print('Initialising tSNE...')
    tsne = TSNE(n_components=2, init='pca', random_state=0, n_iter=5000, perplexity=perplexity)

    print('Fit-transform embeddings...')
    embeddings_tsne_transformed = tsne.fit_transform(embeddings_pca_transformed)

    return embeddings_tsne_transformed


def visualise_embeddings(embeddings, df, perplexity):
    '''
    Visualize embeddings in 2D.

    :param embeddings: pandas dataframe containging embedding vectors
    :param df: pandas dataframe with labels
    :param perplexity: int, parameter for tSNE
    :return: none
    '''
    print('Performing PCA...')
    embeddings_pca_transformed = perform_pca(embeddings)

    print('Performing tSNE...')
    embeddings_tsne_transformed = perform_tsne(embeddings_pca_transformed, perplexity)

    print('Creating dataframe from tSNE results...')
    reduced_data = pd.DataFrame(embeddings_tsne_transformed, columns=['dimension_1', 'dimension_2'])

    print('Adding label to dataframe')
    df = df.reset_index(drop=True)
    reduced_data['label'] = df['label']

    print('Creating figure...')
    plt.figure()
    sns_plot = sns.scatterplot(data=reduced_data, x="dimension_1", y="dimension_2", hue="label", alpha=0.8, s=8)

    # Put the legend out of the figure
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    print('Saving figure...')
    sns_plot.figure.savefig("../results/tsne_model_" + str(config.MODEL) + "_perplexity_" + str(perplexity) + ".png", bbox_inches='tight')

    print('Finished successfully with visualising embeddings!')
