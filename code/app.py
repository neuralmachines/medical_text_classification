import config
import fetch_data
import create_embeddings
import visualise_embeddings
import model_data
import pandas as pd
import logging


# Configure logger
logger = logging.getLogger('logger')
logger.setLevel(logging.INFO)
file_handler = logging.FileHandler('../logfile.log')
formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# Fetching data
df = fetch_data.create_dataset(config.TERMS, config.RETMAX)
# df = pd.read_csv('../data/data.csv')

# Creating embeddings
embeddings = create_embeddings.create_embeddings(df, config.EMBEDDING, config.MODEL, config.MAX_SEQ_LENGTH)
# embeddings = pd.read_csv('../data/embeddings.csv')

# Visualising embeddings with tSNE with different perplexity settings
for perplexity in config.PERPLEXITY:
    visualise_embeddings.visualise_embeddings(embeddings, df, perplexity)

# Preprocess data
X_all, y_all, X_train, X_test, y_train, y_test, label_encoder, class_weights_dict = model_data.preprocess_data(df, embeddings, config.MULTICLASS_FLAG)

# Perform 10-fold crossvalidation
model_data.perform_k_fold_crossvalidation(X_all, y_all, class_weights_dict, config.MULTICLASS_FLAG)

# Train model
results = model_data.train_model(X_train, y_train, X_test, y_test, df, label_encoder, class_weights_dict, config.MULTICLASS_FLAG)
# results = pd.read_csv('../data/test_results.csv')

# Create reports
model_data.create_reports(results)
