import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import logging
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from sklearn.metrics import classification_report
import config


logger = logging.getLogger('logger')


def create_multilabel_targets(label):
    '''
    Create multilabel targets.

    :param label: string
    :return: numpy array
    '''
    if label == 'congenital_abnormalities':
        return np.array([1, 0, 0])
    elif label == 'drug_related_side_effects':
        return np.array([0, 1, 0])
    elif label == 'drug_usage_congenital_abnormalities':
        return np.array([1, 1, 0])
    else:
        return np.array([0, 0, 1])


def preprocess_data(df, embeddings, multiclass):
    '''
    Preprocess data.

    :param df: pandas dataframe
    :param embeddings: pandas dataframe
    :param multiclass: boolean
    :return: pandas dataframes and label_encoder
    '''

    # Join data
    df = embeddings.join(df)

    # Get class weights for Keras
    class_weights = class_weight.compute_class_weight('balanced', np.unique(df['label']), df['label'])
    class_weights_dict = dict(enumerate(class_weights))

    # Fit label encoder
    label_encoder = LabelEncoder()
    df['label_encoded'] = label_encoder.fit_transform(df[["label"]])

    # Fit one hot encoder
    ohe = OneHotEncoder()
    ohe.fit(df[['label_encoded']])

    # Select features
    X_all = df.iloc[:,:-3]

    print('Creating target vector..')
    if multiclass:
        y_all = ohe.transform(df[['label_encoded']]).toarray()
    else:
        df['multilabel_target'] = df['label'].apply(lambda x: create_multilabel_targets(x))
        y_all = np.array(df['multilabel_target'].tolist())

    print('Creating train test split...')
    X_train, X_test, y_train, y_test = train_test_split(X_all, y_all, train_size=0.8, random_state=42, stratify=y_all)
    print('Model will be trained on {} samples and tested on {} samples.'.format(len(X_train), len(X_test)))

    return X_all, y_all, X_train, X_test, y_train, y_test, label_encoder, class_weights_dict


def create_baseline_model_multiclass():
    '''
    Create baseline model for multiclass classification setting.

    :return: keras model
    '''
    print('Creating model...')
    if config.BASELINE_MODEL:
        model = Sequential()
        model.add(Dense(config.INPUT_DIM,
                        input_dim=config.OUTPUT_DIM,
                        kernel_initializer='he_normal'))
        model.add(Activation('softmax'))

    else:
        model = Sequential()
        model.add(Dense(512, input_dim=config.OUTPUT_DIM, kernel_initializer='he_normal'))
        model.add(Activation('relu'))
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(config.INPUT_DIM))
        model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model


def create_baseline_model_multilabel():
    '''
    Create baseline model for multilabel classification setting.

    :return: keras model
    '''
    print('Creating model...')
    if config.BASELINE_MODEL:
        model = Sequential()
        model.add(Dense(config.INPUT_DIM,
                        input_dim=config.OUTPUT_DIM,
                        kernel_initializer='he_normal'))
        model.add(Activation('sigmoid'))

    else:
        model = Sequential()
        model.add(Dense(512, input_dim=config.OUTPUT_DIM, kernel_initializer='he_normal'))
        model.add(Activation('relu'))
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(config.INPUT_DIM))
        model.add(Activation('sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model


def perform_k_fold_crossvalidation(X_all, y_all, class_weights_dict, multiclass):
    '''
    Perform k-fold crossvalidation.

    :param X_all: pandas dataframe
    :param y_all: pandas dataframe
    :param multiclass: boolean
    :return: none
    '''

    print('Performing k-fold crossvalidation...')
    if multiclass:
        estimator = KerasClassifier(build_fn=create_baseline_model_multiclass, epochs=config.EPOCHS, batch_size=config.BATCH_SIZE, verbose=0)
    else:
        estimator = KerasClassifier(build_fn=create_baseline_model_multilabel, epochs=config.EPOCHS, batch_size=config.BATCH_SIZE,
                                    verbose=0)
    kfold = KFold(n_splits=2, shuffle=True)
    # kfold = StratifiedKFold(n_splits=10, shuffle=True)
    # results = cross_val_score(estimator, X_all, y_all, cv=kfold, fit_params = {'class_weight': class_weights_dict})
    results = cross_val_score(estimator, X_all, y_all, cv=kfold)
    print("Crossvalidation result: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
    logger.info("Crossvalidation result %s: %.2f%% (%.2f%%)" % (config.MODEL, results.mean()*100, results.std()*100))

    print('Finished!')


def train_model(X_train, y_train, X_test, y_test, df, label_encoder, class_weights_dict,  multiclass):
    '''
    Train keras model.

    :param X_train: pandas dataframe with training features
    :param y_train: pandas dataframe with training labels
    :param X_test: pandas dataframe with test features
    :param y_test: pandas dataframe with test labels
    :param df: pandas dataframe
    :param label_encoder: scikit-learn label encoder instance
    :param multiclass: boolean
    :return: pandas dataframe
    '''
    print('Initializing model...')
    if multiclass:
        model = create_baseline_model_multiclass()
    else:
        model = create_baseline_model_multilabel()

    print('Fitting model...')
    model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=config.EPOCHS, batch_size=config.BATCH_SIZE,
              verbose=2)
    # model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=config.EPOCHS, batch_size=config.BATCH_SIZE,
    #           verbose=2, class_weight=class_weights_dict)

    print('Creating predictions on test set and save to DataFrame...')
    if multiclass:
        result_df = pd.DataFrame()
        result_df['predictions'] = np.argmax(model.predict(X_test), axis=1)
    else:
        result_df = pd.DataFrame()
        result_df['predictions'] = (model.predict(X_test) > 0.5).astype("int32").tolist()

    print('Merging test results to test dataframe...')
    result_df = result_df.join(df.loc[X_test.index].reset_index(drop=True))

    print('Inverse transforming labels...')
    if multiclass:
        result_df['predicted_label'] = label_encoder.inverse_transform(result_df['predictions'])
    else:
        result_df['predicted_label'] = result_df['predictions'].apply(lambda x: inverse_transfor_multilabel_targets(x))

    print('Exporting results...')
    result_df.to_csv('../data/test_results.csv', index=False)

    print('Exporting model...')
    model.save("../models/model")

    return result_df


def inverse_transfor_multilabel_targets(label):
    '''
    Inverse transform multilabel targets.

    :param label: list
    :return: string
    '''
    if label == [1, 0, 0]:
        return 'congenital_abnormalities'
    elif label == [0, 1, 0]:
        return 'drug_related_side_effects'
    elif label == [1, 1, 0]:
        return 'drug_usage_congenital_abnormalities'
    else:
        return 'others'


def create_reports(df):
    '''
    Create reports and metrics about classifier performance.

    :param df: pandas dataframe
    :return: none
    '''

    # Fetching labels
    labels = sorted(df['label'].unique())

    print('Calculating confusion matrix...')
    cm = confusion_matrix(df['label'], df['predicted_label'], labels=labels)

    print('Plotting confusion matrix...')
    plt.figure()
    sns_plot = sns.heatmap(cm, annot=True, fmt='.0f', linewidths=.5, cmap="YlGnBu",
                           xticklabels=labels,
                           yticklabels=labels).set_title(config.MODEL)

    print('Saving figure...')
    sns_plot.figure.savefig("../results/confusion_matrix_" + str(config.MODEL) + ".png", bbox_inches='tight')

    print('Creating and exporting classification report...')
    pd.DataFrame(
        classification_report(df['label'], df['predicted_label'], target_names=labels, output_dict=True)).to_csv(
        '../results/classification_report_' + str(config.MODEL) + '.csv')
