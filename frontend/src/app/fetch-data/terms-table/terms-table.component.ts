import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: string;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 'congenital_abnormalities', name: 'Congenital Abnormalities[mesh]+NOT+Drug-Related Side Effects and Adverse Reactions[mesh]', weight: 1.0079, symbol: 'H' },
  { position: 'drug_related_side_effects', name: 'Drug-Related Side Effects and Adverse Reactions[mesh]+NOT+Congenital Abnormalities[mesh]', weight: 4.0026, symbol: 'He' },
  { position: 'drug_usage_congenital_abnormalities', name: 'Congenital Abnormalities[mesh]+AND+Drug-Related Side Effects and Adverse Reactions[mesh]', weight: 6.941, symbol: 'Li' },
  { position: 'psycholinguistics', name: 'Psycholinguistics[mesh]', weight: 9.0122, symbol: 'Be' },
];

@Component({
  selector: 'app-terms-table',
  templateUrl: './terms-table.component.html',
  styleUrls: ['./terms-table.component.css']
})
export class TermsTableComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'edit'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
