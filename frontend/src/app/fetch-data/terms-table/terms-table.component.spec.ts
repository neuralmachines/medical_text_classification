import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsTableComponent } from './terms-table.component';

describe('TermsTableComponent', () => {
  let component: TermsTableComponent;
  let fixture: ComponentFixture<TermsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
