import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from './app-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { BackendApiService } from './backend-api.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from "./app.component";
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { TermsTableComponent } from './fetch-data/terms-table/terms-table.component';
import { CreateEmbeddingsComponent } from './navigation/create-embeddings/create-embeddings.component';
import { ModelSelectorComponent } from './navigation/create-embeddings/model-selector/model-selector.component';
import { FooterComponent } from './footer/footer.component';
import { VisualizeEmbeddingsComponent } from './navigation/visualize-embeddings/visualize-embeddings.component';
import { ModelDataComponent } from './navigation/model-data/model-data.component';
import { ExploreResultsComponent } from './navigation/explore-results/explore-results.component';
import { ClassificationReportTableComponent } from './navigation/explore-results/classification-report-table/classification-report-table.component';
import { CrossvalidationReportTableComponent } from './navigation/explore-results/crossvalidation-report-table/crossvalidation-report-table.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, NavigationComponent, FetchDataComponent, TermsTableComponent, CreateEmbeddingsComponent, ModelSelectorComponent, FooterComponent, VisualizeEmbeddingsComponent, ModelDataComponent, ExploreResultsComponent, ClassificationReportTableComponent, CrossvalidationReportTableComponent],
  imports: [BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
