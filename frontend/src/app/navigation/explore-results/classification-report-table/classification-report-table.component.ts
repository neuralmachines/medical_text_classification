import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  position: string;
  precision: string;
  recall: string;
  f1score: string;
  support: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 'congenital_abnormalities', precision: '0.95', recall: '0.87', f1score: '0.91', support: '500' },
  { position: 'drug_related_side_effects', precision: '0.81', recall: '0.94', f1score: '0.87', support: '500' },
  { position: 'drug_usage_congenital_abnormalities', precision: '0.75', recall: '0.46', f1score: '0.57', support: '110' },
  { position: 'psycholinguistics', precision: '0.99', recall: '0.99', f1score: '0.99', support: '500' },
  { position: '', precision: '', recall: '', f1score: '', support: '' },
  { position: 'accuracy', precision: '', recall: '', f1score: '0.90', support: '1610' },
  { position: 'macro avg', precision: '0.87', recall: '0.82', f1score: '0.84', support: '1610' },
  { position: 'weighted avg', precision: '0.90', recall: '0.90', f1score: '0.90', support: '1610' }
];

@Component({
  selector: 'app-classification-report-table',
  templateUrl: './classification-report-table.component.html',
  styleUrls: ['./classification-report-table.component.css']
})
export class ClassificationReportTableComponent implements OnInit {
  displayedColumns: string[] = ['position', 'precision', 'recall', 'f1score', 'support'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
