import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationReportTableComponent } from './classification-report-table.component';

describe('ClassificationReportTableComponent', () => {
  let component: ClassificationReportTableComponent;
  let fixture: ComponentFixture<ClassificationReportTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassificationReportTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationReportTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
