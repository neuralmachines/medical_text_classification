import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrossvalidationReportTableComponent } from './crossvalidation-report-table.component';

describe('CrossvalidationReportTableComponent', () => {
  let component: CrossvalidationReportTableComponent;
  let fixture: ComponentFixture<CrossvalidationReportTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrossvalidationReportTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrossvalidationReportTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
