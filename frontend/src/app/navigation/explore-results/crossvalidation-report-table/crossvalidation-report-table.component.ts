import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  embedding: string;
  model: string;
  dimension: number;
  epoch: number;
  max_seq_length: number;
  accuracy: number;
  sd: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { embedding: 'albert', model: 'albert_base', dimension: 768, epoch: 20, max_seq_length: 256, accuracy: 87.49, sd: 1.42 },
  { embedding: 'albert', model: 'albert_large', dimension: 1024, epoch: 20, max_seq_length: 256, accuracy: 87.84, sd: 1.3 },
  { embedding: 'albert', model: 'albert_xlarge', dimension: 2048, epoch: 20, max_seq_length: 256, accuracy: 88.61, sd: 0.76 },
  { embedding: 'albert', model: 'albert_xxlarge', dimension: 4096, epoch: 20, max_seq_length: 128, accuracy: 90.64, sd: 0.98 },
  { embedding: 'albert', model: 'albert_xxlarge', dimension: 4096, epoch: 20, max_seq_length: 192, accuracy: 90.28, sd: 0.7 },
  { embedding: 'albert', model: 'albert_xxlarge', dimension: 4096, epoch: 100, max_seq_length: 192, accuracy: 91.28, sd: 1.09 },
];

@Component({
  selector: 'app-crossvalidation-report-table',
  templateUrl: './crossvalidation-report-table.component.html',
  styleUrls: ['./crossvalidation-report-table.component.css']
})
export class CrossvalidationReportTableComponent implements OnInit {
  displayedColumns: string[] = ['embedding', 'model', 'dimension', 'epoch', 'max_seq_length', 'accuracy', 'sd'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
