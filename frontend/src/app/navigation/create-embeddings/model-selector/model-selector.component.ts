import { Component, OnInit } from '@angular/core';

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-model-selector',
  templateUrl: './model-selector.component.html',
  styleUrls: ['./model-selector.component.css']
})
export class ModelSelectorComponent implements OnInit {

  foods: Food[] = [
    { value: 'steak-0', viewValue: 'Albert base' },
    { value: 'pizza-1', viewValue: 'Albert large' },
    { value: 'tacos-2', viewValue: 'Albert xlarge' },
    { value: 'tacos-3', viewValue: 'Albert xxlarge' }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
