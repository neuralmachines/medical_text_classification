import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmbeddingsComponent } from './create-embeddings.component';

describe('CreateEmbeddingsComponent', () => {
  let component: CreateEmbeddingsComponent;
  let fixture: ComponentFixture<CreateEmbeddingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmbeddingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmbeddingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
