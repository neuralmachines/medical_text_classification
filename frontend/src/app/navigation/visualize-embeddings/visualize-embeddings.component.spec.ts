import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeEmbeddingsComponent } from './visualize-embeddings.component';

describe('VisualizeEmbeddingsComponent', () => {
  let component: VisualizeEmbeddingsComponent;
  let fixture: ComponentFixture<VisualizeEmbeddingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeEmbeddingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeEmbeddingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
