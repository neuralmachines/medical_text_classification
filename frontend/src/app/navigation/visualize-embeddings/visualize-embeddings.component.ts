import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visualize-embeddings',
  templateUrl: './visualize-embeddings.component.html',
  styleUrls: ['./visualize-embeddings.component.css']
})
export class VisualizeEmbeddingsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
