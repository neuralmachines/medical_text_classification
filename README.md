# Medical Text Classification

Please check a running application with the results at: [https://prod.d3pc6j3ds8hjs.amplifyapp.com/](https://prod.d3pc6j3ds8hjs.amplifyapp.com/)

### Features of frontend app:
* Show search terms and target categories that will be fetched from PubMed
* Select model type for training
* Show tSNE embeddings that can be selected from dropdown list
* Select model hyperparameters
* Show model performance

### TODO:
* The frontend is not yet configured with python backend i.e. you cannot run jobs from the frontend
* extend hyperparameter settings
* extend model selection list

## Python setup

By running `python app.py` the app will:

* Fetch abstracts from PubMed
* Create embeddings
* Perform PCA and visualize embeddings with tSNE
* Preprocess the data
* Perform 10-fold crossvalidation and 
* Create reports.

For `app.py` parameters can be set from `config.py`. The main parameters are:

* `RETMAX:` number of abstracts to fetch per category
* `TERMS:` a dictionary containing search parameters to fetch abstracts
* `EMBEDDING:` type of embedding model to use `[albert]`
* `MODEL:` embedding model subtype `[albert_base, albert_large, albert_xlarge, albert_xxlarge]`
* `MAX_SEQ_LENGTH:` max sequence length that will be used during embedding
* `MULTICLASS_FLAG:` set to `True` if you want the model to run in multiclass setting. Otherwise it runs in multilabel setting.
* `EPOCHS:` number of epochs to train the neural network

### Recommended hardware:
The embeddings were created on AWS with the following AMI: 

* Deep Learning AMI (Ubuntu 18.04) Version 39.0.

Instance type: 

* p3.2xlarge - Nvidia Tesla V100 (16Gb)


## Frontend setup

The frontend part was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

Please navigate to `frontend` folder and run `npm install`.

### Development server

Please run `ng serve` for a dev server then navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

A running application is also available at: [https://prod.d3pc6j3ds8hjs.amplifyapp.com/](https://prod.d3pc6j3ds8hjs.amplifyapp.com/)

### Further help

To get help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


